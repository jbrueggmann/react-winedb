import * as React from "react";
import Alcohol from "../models/Alcohol";

export interface AlcoholFormProps {
    bottle: Alcohol;
}

export interface AlcoholFormState {
    editedBottle: Alcohol;
}

export default class AlcoholForm extends React.Component<AlcoholFormProps, AlcoholFormState> {

    constructor(props: AlcoholFormProps) {
        super(props);

        this.state = {
            editedBottle: Object.assign({}, props.bottle)
        };
    }

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {

        console.log("Editing bottle name", e.target.value);

        const newBottle = Object.assign({}, this.state.editedBottle);
        newBottle.name = e.target.value;
        this.setState((prevState) => {
            return { editedBottle: newBottle };
        });
    }

    render() {
        return (
            <div>
                <input value={this.state.editedBottle.name} onChange={this.handleChange} />
            </div>
        );
    }
}