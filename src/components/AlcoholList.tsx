import * as React from "react";
import Alcohol from "../models/Alcohol";

interface AlcoholListProps {
    bottles: Alcohol[];
    stashName: string;
    bottleSelected: any;
}

export default class AlcoholList extends React.Component<AlcoholListProps, {}> {

    handleClick = (selectedBottle: Alcohol) => {
        this.props.bottleSelected(selectedBottle);
    }

    render() {

        const newArray = this.props.bottles.map((bottle) => { return <p key={bottle.name} onClick={() => this.handleClick(bottle)}>{bottle.name}</p>; } );
        console.log("our new array looks like this", newArray);

        return (
            <div>
                {this.props.stashName}
                {
                    newArray
                }

                {
                    this.props.children
                }

            </div>
        );
    }
}