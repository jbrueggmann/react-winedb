import * as React from "react";
import "./../assets/scss/App.scss";
import AlcoholList from "./AlcoholList";
import Alcohol from "../models/Alcohol";
import AlcoholForm from "./AlcoholForm";

export interface AppProps {
}

export interface AppState {
    stash: Alcohol[];
    stashName: string;
    selectedBottle?: Alcohol;
}

export default class App extends React.Component<AppProps, AppState> {

    constructor(props: AppProps) {
        super(props);
        this.state = {
            stash: [
            {
                name: "Catarratto",
                percentage: 12.5,
                alcoholType: "Wine"
            },
            {
                name: "Dalmore",
                percentage: 40,
                alcoholType: "Whisky"
            }],
            stashName: "Jans stash",
            selectedBottle: undefined
        };
    }

    onBottleSelected = (bottle: Alcohol) => {
        console.log("We have selected a new bottle!", bottle);
        this.setState((prevState) => {
            return { selectedBottle: bottle};
        });
    }

    render() {

        return (
            <div className="container">
                <div>
                    <AlcoholList bottles={this.state.stash} stashName={this.state.stashName} bottleSelected={this.onBottleSelected}>
                        <div>I'm using the composition pattern</div>
                    </AlcoholList>
                </div>
                <div></div>
                <div>
                    {
                        this.state.selectedBottle && <AlcoholForm key={this.state.selectedBottle.name} bottle={this.state.selectedBottle}></AlcoholForm>
                    }
                </div>
            </div>
        );
    }
}
