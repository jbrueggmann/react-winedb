export default interface Alcohol {
    name: string;
    percentage: number;
    alcoholType: "Rum" | "Whisky" | "Wine" | "Beer";
}